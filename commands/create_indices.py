#!/usr/bin/env python

import sys
import sqlite3


def main(database, table_name):
    columns = get_column_names(database, table_name)
    for column in columns:
        create_index(database, table_name, column)


def execute(sql, db_file='scraperwiki.sqlite', read_only=False):
    """
    Executes a SQL command using sqlite3 and returns the result. Setting
    read_only to True means the commit step is omitted.
    """
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    result = cursor.execute(sql)
    if not read_only:
        conn.commit()
        conn.close()
    return result


def get_column_names(db_file, table):
    """
    Return a list of column names from the given table in the sqlite database.
    """
    result = execute("SELECT * FROM `%s` LIMIT 0;" % table, db_file, True)

    if result.description:
        column_names = [col[0] for col in result.description]
    return column_names


def create_index(db_file, table, column):
    """
    Create a single index for the given database, table and column combination.
    """
    command = "CREATE INDEX IF NOT EXISTS `sw_%s_%s` ON `%s` (`%s`);" % (
        table, column, table, column)
    print(command)
    execute(command)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: %s <database.sql> <table>" % sys.argv[0])
        sys.exit(1)
    main(sys.argv[1], sys.argv[2])
